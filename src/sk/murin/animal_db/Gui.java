package sk.murin.animal_db;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Gui extends JFrame {
    private final JLabel[] labels = new JLabel[4];
    private final Font f = new Font("Calibri", Font.BOLD, 18);
    private JTextField fieldAnimalType;
    private JTextField fieldAge;
    private JRadioButton buttonMale;
    private JRadioButton buttonFemale;
    private JCheckBox jCheckBoxOutside;
    private JCheckBox jCheckBoxInside;
    private JLabel labelId;
    private JTable table;
    private DefaultTableModel modelTable;


    public Gui() {
        setup();
    }

    private void setup() {
        setTitle("Db");
        setSize(820, 350);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);


        setLayout(new BorderLayout());
        add(makePanelLeft(), BorderLayout.WEST);
        add(makePanelRight(), BorderLayout.EAST);
        try {
            Db.getInstance().connect();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        showAnimals();
        setVisible(true);

    }

    private JPanel makePanelLeft() {
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(makePanelLeftTop(), BorderLayout.NORTH);
        panel.add(makePanelLeftCenter(), BorderLayout.CENTER);
        panel.add(makePanelLeftBottom(), BorderLayout.SOUTH);
        panel.setPreferredSize(new Dimension(280, 350));

        Border blackBorder = BorderFactory.createLineBorder(Color.black);
        panel.setBorder(blackBorder);

        return panel;
    }


    private JPanel makePanelLeftTop() {
        JPanel panel = new JPanel();
        JLabel labelTitle = new JLabel("Add an animal");
        panel.add(labelTitle);
        return panel;
    }

    private JPanel makePanelLeftCenter() {
        JPanel panel = new JPanel();
        panel.setLayout(null);
        for (int i = 0; i < labels.length; i++) {
            labels[i] = new JLabel();
            labels[i].setFont(f);
        }

        labels[0] = new JLabel("Animal type: ");
        labels[0].setBounds(10, 55, 130, 30);
        panel.add(labels[0]);

        labels[1] = new JLabel("Gender: ");
        labels[1].setBounds(10, 105, 100, 30);
        panel.add(labels[1]);


        labels[2] = new JLabel("Occurence: ");
        labels[2].setBounds(10, 145, 130, 30);
        panel.add(labels[2]);

        labels[3] = new JLabel("Age : ");
        labels[3].setBounds(10, 190, 130, 30);
        panel.add(labels[3]);


        fieldAnimalType = new JTextField();
        fieldAnimalType.setBounds(90, 55, 150, 30);
        fieldAnimalType.setFont(f);
        panel.add(fieldAnimalType);

        buttonFemale = new JRadioButton("female");
        buttonFemale.setBounds(120, 105, 80, 30);
        panel.add(buttonFemale);
        buttonMale = new JRadioButton("male");
        buttonMale.setBounds(200, 105, 130, 30);
        panel.add(buttonMale);
        buttonMale.setSelected(true);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(buttonFemale);
        buttonGroup.add(buttonMale);

        jCheckBoxOutside = new JCheckBox("Outside");
        jCheckBoxOutside.setBounds(120, 145, 75, 30);
        panel.add(jCheckBoxOutside);
        jCheckBoxInside = new JCheckBox("Inside");
        jCheckBoxInside.setBounds(200, 145, 80, 30);
        panel.add(jCheckBoxInside);

        JCheckBox[] checkBoxes = new JCheckBox[2];
        checkBoxes[0] = jCheckBoxOutside;
        checkBoxes[1] = jCheckBoxInside;

        fieldAge = new JTextField();
        fieldAge.setBounds(125, 190, 90, 30);
        fieldAge.setFont(f);
        panel.add(fieldAge);

        return panel;
    }

    private JPanel makePanelLeftBottom() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));
        JButton buttonSave = new JButton("Save");
        buttonSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (labelId.getText().isEmpty()) {
                    addAnimalGui();
                } else {
                    editAnimalGui();
                }
            }
        });
        JButton buttonDelete = new JButton("Delete");
        buttonDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteAnimalGui();
            }
        });
        panel.add(buttonSave);
        panel.add(buttonDelete);
        JLabel labelIdTeXT = new JLabel(" Animals id :");
        labelIdTeXT.setBounds(35, 290, 100, 50);
        Font fNew = new Font("Calibri", Font.BOLD, fieldAnimalType.getFont().getSize() - 4);

        labelIdTeXT.setFont(fNew);
        panel.add(labelIdTeXT);

        labelId = new JLabel("");
        labelId.setBounds(160, 290, 100, 50);
        labelId.setFont(f);

        panel.add(labelId);
        return panel;
    }

    private JPanel makePanelRight() {
        JPanel panelRight = new JPanel();
        panelRight.setPreferredSize(new Dimension(525, 350));
        panelRight.setLayout(new GridLayout(1, 1));

        table = new JTable(0, 5) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column != 0;
            }
        };

        table.setPreferredScrollableViewportSize(new Dimension
                (panelRight.getWidth(), panelRight.getHeight()));
        table.setBackground(new Color(200, 200, 200));
        table.setFillsViewportHeight(true);
        table.setDefaultEditor(Object.class, null);
        table.setRowHeight(22);

        Dimension tableSize = table.getPreferredSize();
        int x = tableSize.width;
        table.getColumnModel().getColumn(0).setPreferredWidth(Math.round(x * 0.10f));
        table.getColumnModel().getColumn(1).setPreferredWidth(Math.round(x * 0.30f));
        table.getColumnModel().getColumn(2).setPreferredWidth(Math.round(x * 0.20f));
        table.getColumnModel().getColumn(3).setPreferredWidth(Math.round(x * 0.30f));
        table.getColumnModel().getColumn(4).setPreferredWidth(Math.round(x * 0.10f));

        JTableHeader header = table.getTableHeader();
        TableColumnModel colMod = header.getColumnModel();
        TableColumn col0 = colMod.getColumn(0);
        col0.setResizable(false);
        col0.setHeaderValue("id");

        TableColumn col1 = colMod.getColumn(1);
        col1.setResizable(false);
        col1.setHeaderValue("Animal type");
        TableColumn col2 = colMod.getColumn(2);
        col2.setResizable(false);
        col2.setHeaderValue("Gender");
        TableColumn col3 = colMod.getColumn(3);
        col3.setResizable(false);
        col3.setHeaderValue("Occurence");
        TableColumn col4 = colMod.getColumn(4);
        col4.setResizable(false);
        col4.setHeaderValue("Age");

        modelTable = (DefaultTableModel) table.getModel();

        ListSelectionModel model = table.getSelectionModel();
        model.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                int i = table.getSelectedRow();
                if (i == -1) {
                    return;
                }
                labelId.setText(modelTable.getValueAt(i, 0).toString());
                fieldAnimalType.setText(modelTable.getValueAt(i, 1).toString());
                String gender = modelTable.getValueAt(i, 2).toString();
                if (gender.equalsIgnoreCase("Male")) {
                    buttonMale.setSelected(true);
                } else {
                    buttonFemale.setSelected(true);
                }
                String occurence = modelTable.getValueAt(i, 3).toString();
                switch (occurence) {
                    case "Inside ":
                        jCheckBoxInside.setSelected(true);
                        jCheckBoxOutside.setSelected(false);
                        break;
                    case "Outside ":
                        jCheckBoxInside.setSelected(false);
                        jCheckBoxOutside.setSelected(true);
                        break;
                    default:
                        jCheckBoxInside.setSelected(true);
                        jCheckBoxOutside.setSelected(true);
                        break;
                }
                String age = String.valueOf(modelTable.getValueAt(i, 4));
                fieldAge.setText(age);

            }
        });

        table.grabFocus();
        table.getTableHeader().setReorderingAllowed(false);
        panelRight.add(new JScrollPane(table));

        return panelRight;
    }

    public void addAnimalGui() {
        if (!checkRight()) {
            JOptionPane.showMessageDialog(null, "Enter valid input!", "!", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        try {
            String animalType = fieldAnimalType.getText();
            String gender;
            if (buttonFemale.isSelected()) {
                gender = "Female";
            } else {
                gender = "Male";
            }
            String occurence = "";
            if (jCheckBoxInside.isSelected()) {
                occurence += jCheckBoxInside.getText() + " ";
            }
            if (jCheckBoxOutside.isSelected()) {
                occurence += jCheckBoxOutside.getText() + " ";
            } else if (!jCheckBoxOutside.isSelected() && !jCheckBoxInside.isSelected()) {
                occurence = " - ";
            }
            String age = String.valueOf(fieldAge.getText());

            if (!animalType.isEmpty() && !age.isEmpty()) {//tu sa pozret
                int id = Db.getInstance().addAnimal(new Animal(0, fieldAnimalType.getText(), gender, occurence, Integer.parseInt(fieldAge.getText())));
                if (Integer.parseInt(fieldAge.getText()) <= 0) {
                    return;
                }
                if (id != -1) {
                    modelTable.addRow(new Object[]{id, animalType, gender, occurence, age});
                }
            } else {
                JOptionPane.showMessageDialog(null, "Enter correct data...", "Correct your inputs..", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (NullPointerException exc) {
            exc.fillInStackTrace();
        }
        reset();
    }

    public void reset() {
        fieldAnimalType.setText("");
        fieldAge.setText("");
        jCheckBoxInside.setSelected(false);
        jCheckBoxOutside.setSelected(false);
        labelId.setText("");
    }

    public void showAnimals() {
        ArrayList<Animal> animals = Db.getInstance().loadAnimals();
        while (modelTable.getRowCount() > 0) {
            modelTable.removeRow(0);
        }

        for (int i = 0; i < animals.size(); i++) {
            Object[] rowData = new Object[5];

            rowData[0] = animals.get(i).id;
            rowData[1] = animals.get(i).animalType;
            rowData[2] = animals.get(i).gender;
            rowData[3] = animals.get(i).occurence;
            rowData[4] = animals.get(i).age;
            modelTable.addRow(rowData);
        }
    }

    public void deleteAnimalGui() {
        if (table.getSelectedRow() == -1) {
            return;
        }
        int id = (int) modelTable.getValueAt(table.getSelectedRow(), 0);
        boolean successDelete = Db.getInstance().deleteAnimal(id);
        if (successDelete) {
            modelTable.removeRow(table.getSelectedRow());
            reset();
        }

    }

    public void editAnimalGui() {
        if (!checkRight()) {
            JOptionPane.showMessageDialog(null, "You entered wrong input..", "Correct your inputs..", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String occurence = "";
        if (jCheckBoxInside.isSelected()) {
            occurence += jCheckBoxInside.getText() + " ";
        }
        if (jCheckBoxOutside.isSelected()) {
            occurence += jCheckBoxOutside.getText() + " ";
        } else if (!jCheckBoxOutside.isSelected() && !jCheckBoxInside.isSelected()) {
            occurence = " - ";
        }

        Db.getInstance().editAnimal(new Animal(Integer.parseInt(labelId.getText()), fieldAnimalType.getText(),
                buttonMale.isSelected() ? "Male" : "Female", occurence, Integer.parseInt(fieldAge.getText())));

        reset();
        showAnimals();
    }


    private boolean checkRight() {
        String animalType = fieldAnimalType.getText();
        String age = fieldAge.getText();

        if (!animalType.matches("[a-z A-Z]+")) {      // tieto znaky tu musia byt
            return false;
        }
        if (!age.matches("[0-9][0-9]?")) {            // tu musi byt len 0-2 ciferne cislo
            return false;
        }
        return true;
    }


}
