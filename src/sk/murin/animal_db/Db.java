package sk.murin.animal_db;


import java.sql.*;
import java.util.ArrayList;

public class Db {
    private static Db db;
    private Connection con;

    private Db() {
    }

    public static Db getInstance() {

        if (null == db) {
            db = new Db();
        }
        return db;
    }

    public void connect() throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/animal_db", "Palo", "root");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public int addAnimal(Animal a) {
        try {
            PreparedStatement st = con.prepareStatement("INSERT INTO animal_table" +
                    " (animal_type,gender,occurence,age)VALUES (?,?,?,?)");
            st.setString(1, a.animalType);
            st.setString(2, a.gender);
            st.setString(3, a.occurence);
            st.setInt(4, a.age);
            int n = st.executeUpdate();
             ResultSet set = st.getGeneratedKeys();
            if (set.next()) {
                return set.getInt(1);
            } else {
                return -1;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }


    }

    public ArrayList<Animal> loadAnimals() {
        try {
            Statement st = con.createStatement();
            ResultSet resultSet = st.executeQuery("SELECT * FROM animal_table");
            ArrayList<Animal> animals = new ArrayList<>();

            while (resultSet.next()) {
                animals.add(new Animal(resultSet.getInt("id"), resultSet.getString("animal_type"), resultSet.getString("gender"),
                        resultSet.getString("occurence"), resultSet.getInt("age")));
            }
            return animals;
        } catch (SQLException throwables) {
            return null;
        }
    }

    public boolean editAnimal(Animal a) {
        try {
            PreparedStatement ps = con.prepareStatement("UPDATE animal_table SET animal_type=?,gender=?,occurence=?, age=?" +
                    " WHERE id=?");

            ps.setString(1, a.animalType);
            ps.setString(2, a.gender);
            ps.setString(3, a.occurence);
            ps.setInt(4, a.age);
            ps.setInt(5, a.id);
             int n = ps.executeUpdate();
            System.out.println(n);

            return n == 1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    public boolean deleteAnimal(int id) {
        try {
            PreparedStatement pst = con.prepareStatement("DELETE FROM animal_table where id=?");
            pst.setInt(1, id);
            int n = pst.executeUpdate();
            System.out.println(n);

            return n == 1;
        } catch (
                SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }

    }





}
