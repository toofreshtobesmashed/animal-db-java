package sk.murin.animal_db;

public class Animal {
    public int id;
    public String animalType;
    public String gender;
    public String occurence;
    public int age;


    public Animal(int id, String animalType, String gender, String occurence, int age) {
        this.id = id;
        this.animalType = animalType;
        this.gender = gender;
        this.occurence = occurence;
        this.age = age;

    }

    @Override
    public String toString() {
        return id + " " + animalType + " " + gender + " " + occurence + " " + age;
    }
}
